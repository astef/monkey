﻿using System;
using System.Text;
using Monkey.Client.MonkeyStorage;

namespace Monkey.Client
{
    class Program
    {
        private static void Main(string[] args)
        {
            var client = new StorageClient();

            PrintQueryResult(client.Execute(new QueryBatch
            {
                Queries = new Query[]
                {
                    new SetQuery
                    {
                        Key = 1,
                        Value = Encoding.UTF8.GetBytes("adinadin")
                    },
                    new SetQuery
                    {
                        Key = 2,
                        Value = Encoding.UTF8.GetBytes("dvadva")
                    },
                    new SetQuery
                    {
                        Key = 3,
                        Value = Encoding.UTF8.GetBytes("tritri")
                    },
                    new DropQuery
                    {
                        Key = 2
                    }
                }
            }));

            PrintQueryResult(client.Execute(new QueryBatch
            {
                Queries = new Query[]
                {
                    new GetAllKeysQuery()
                }
            }));

            PrintQueryResult(client.Execute(new QueryBatch
            {
                Queries = new Query[]
                {
                    new GetQuery
                    {
                        Key = 1
                    },
                    new GetQuery
                    {
                        Key = 10
                    }
                }
            }));

            Console.ReadLine();
        }

        private static void PrintQueryResult(QueryResult qr)
        {
            Console.WriteLine();
            foreach (var record in qr.Records)
            {
                var value = record.Value == null ? "<?>" : Encoding.UTF8.GetString(record.Value);
                Console.WriteLine("{0}: {1} \t [{2}]", record.Key, value, record.State);
            }
            Console.WriteLine();
        }
    }
}
