﻿using System;
using System.ServiceModel;

namespace Monkey.Service.ConsoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Base Address
                //var httpBaseAddress = new Uri("http://localhost:4321/StudentService");

                // Instantiate ServiceHost
                //studentServiceHost = new ServiceHost(typeof(Storage), httpBaseAddress);
                var storageServiceHost = new ServiceHost(typeof(Storage));

                // Add Endpoint to Host
                //studentServiceHost.AddServiceEndpoint(typeof(IStorage), new WSHttpBinding(), "");

                // Metadata Exchange
                //var serviceBehavior = new ServiceMetadataBehavior { HttpGetEnabled = true };
                //studentServiceHost.Description.Behaviors.Add(serviceBehavior);

                // Open
                storageServiceHost.Open();
                //Console.WriteLine("Storage started at : {0}", httpBaseAddress);
                Console.WriteLine("Storage started");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("ENTER TO EXIT");
            Console.ReadLine();
        }
    }
}
