﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Monkey.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public sealed class Storage : IStorage
    {
        private readonly Dictionary<Type, Action<QueryResultBuilder, Query>> _queryParsers;

        private readonly Dictionary<int, StorageValue> _storage = new Dictionary<int, StorageValue>();

        public Storage()
        {
            _queryParsers = new Dictionary<Type, Action<QueryResultBuilder, Query>>
            {
                { typeof(GetAllKeysQuery), ParseGetAllKeysQuery },
                { typeof(GetQuery), ParseGetQuery },
                { typeof(SetQuery), ParseSetQuery },
                { typeof(DropQuery), ParseDropQuery },
            };
        }

        public QueryResult Execute(QueryBatch batch)
        {
            if (batch == null || batch.Queries == null || batch.Queries.Length == 0)
                return EmptyResult();
            var qrb = new QueryResultBuilder();
            foreach (var query in batch.Queries)
            {
                var queryType = query.GetType();
                _queryParsers[queryType](qrb, query);
            }
            return qrb.Build();
        }

        private void ParseGetAllKeysQuery(QueryResultBuilder b, Query q)
        {
            //var getAllKeysQuery = (GetAllKeysQuery)q;
            foreach (var key in _storage.Keys)
                b.AddRecord(RecordState.NotChanged, key);
        }

        private void ParseGetQuery(QueryResultBuilder b, Query q)
        {
            var getQuery = (GetQuery)q;
            StorageValue sv;
            _storage.TryGetValue(getQuery.Key, out sv);
            b.AddRecord(RecordState.NotChanged, getQuery.Key, sv);
        }

        private void ParseSetQuery(QueryResultBuilder b, Query q)
        {
            var setQuery = (SetQuery)q;
            StorageValue sv;
            if (!_storage.TryGetValue(setQuery.Key, out sv))
            {
                sv = new StorageValue();
                _storage[setQuery.Key] = sv;
            }
            sv.Set(setQuery.Value);
            b.AddRecord(RecordState.Set, setQuery.Key);
        }

        private void ParseDropQuery(QueryResultBuilder b, Query q)
        {
            var dropQuery = (DropQuery)q;
            _storage.Remove(dropQuery.Key);
            b.AddRecord(RecordState.Deleted, dropQuery.Key);
        }

        private static QueryResult EmptyResult()
        {
            return new QueryResult { Records = new Record[0] };
        }

        class QueryResultBuilder
        {
            private readonly Dictionary<int, Record> _records = new Dictionary<int, Record>();

            public void AddRecord(RecordState state, int key, StorageValue sv = null)
            {
                Record existingRecord;
                if (_records.TryGetValue(key, out existingRecord))
                {
                    existingRecord.State = (RecordState)Math.Max((int)existingRecord.State, (int)state);
                    if (sv != null)
                        existingRecord.Value = sv.Get();
                }
                else
                {
                    _records[key] = new Record { Key = key, State = state, Value = sv == null ? null : sv.Get() };
                }
            }

            public QueryResult Build()
            {
                return new QueryResult { Records = _records.Values.ToArray() };
            }
        }

        class StorageValue
        {
            private byte[] _data = new byte[0];
            public byte[] Get()
            {
                return _data.ToArray();
            }
            public void Set(IEnumerable<byte> data)
            {
                _data = data.ToArray();
            }
        }
    }
}
