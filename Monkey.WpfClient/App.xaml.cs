﻿using System.Windows;

namespace Monkey.WpfClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var vm = new MainWindowViewModel();
            var v = new MainWindow { DataContext = vm };
            v.Show();

            base.OnStartup(e);
        }
    }
}
