﻿using System;

namespace Monkey.WpfClient
{
    public class BatchItem : ViewModel
    {
        private string _stringRepresentation;
        private Query _query;

        public string StringRepresentation
        {
            get { return _stringRepresentation; }
            set
            {
                if (value == _stringRepresentation)
                    return;
                _stringRepresentation = value;
                OnPropertyChanged(() => StringRepresentation);
            }
        }

        public Query Query
        {
            get { return _query; }
            set
            {
                if (Equals(value, _query))
                    return;
                _query = value;
                OnPropertyChanged(() => Query);
            }
        }

        public Action<QueryResult> ResultHandler { get; set; }
    }
}