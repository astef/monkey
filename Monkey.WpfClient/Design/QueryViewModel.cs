﻿using System;
using System.Collections;
using System.Collections.ObjectModel;

namespace Monkey.WpfClient.Design
{
    public class DummyQueryViewModel : QueryViewModel
    {
        private readonly IEnumerable _arguments;

        public DummyQueryViewModel()
            : base(new DummyService())
        {
            _arguments = new object[]
            {
                new KeyArgumentViewModel(),
                new ValueArgumentViewModel()
            };
        }

        public override string QueryName
        {
            get { return "Dummy"; }
        }

        public override string QueryDescription
        {
            get { return "Long description of the query which exists only in design-time"; }
        }

        public override IEnumerable Arguments
        {
            get { return _arguments; }
        }

        protected override BatchItem GetBatchItem()
        {
            throw new NotImplementedException();
        }

        class DummyService : IUiService
        {
            public void Execute(Query query)
            {
                throw new NotImplementedException();
            }

            public void Execute(BatchItem item)
            {
                throw new NotImplementedException();
            }

            public void ExecuteBatch()
            {
                throw new NotImplementedException();
            }

            public ObservableCollection<BatchItem> Batch { get; private set; }

            public ObservableCollection<ResultRow> Results { get; private set; }
        }
    }
}
