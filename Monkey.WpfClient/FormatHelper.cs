﻿using System.Text;

namespace Monkey.WpfClient
{
    public static class FormatHelper
    {
        public static string Deserialize(byte[] value)
        {
            return Encoding.UTF8.GetString(value);
        }

        public static byte[] Serialize(string value)
        {
            return Encoding.UTF8.GetBytes(value);
        }
    }
}
