﻿using System.Collections.ObjectModel;

namespace Monkey.WpfClient
{
    public interface IUiService
    {
        void Execute(BatchItem item);

        void ExecuteBatch();

        ObservableCollection<BatchItem> Batch { get; }

        ObservableCollection<ResultRow> Results { get; }
    }
}
