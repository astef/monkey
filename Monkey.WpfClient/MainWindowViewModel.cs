﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Monkey.WpfClient.MonkeyStorage;

namespace Monkey.WpfClient
{
    public sealed class MainWindowViewModel : ViewModel, IUiService
    {
        private readonly IEnumerable _queries;
        private readonly ObservableCollection<BatchItem> _batch = new ObservableCollection<BatchItem>();
        private readonly ObservableCollection<ResultRow> _results = new ObservableCollection<ResultRow>();

        public MainWindowViewModel()
        {
            _queries = new object[]
            {
                new GetAllKeysQueryViewModel(this),
                new GetQueryViewModel(this),
                new SetQueryViewModel(this),
                new DropQueryViewModel(this)
            };
            ExecuteBatchCommand = new DelegateCommand(a => ExecuteBatch(), a => Batch.Count > 0);
            ClearResultsCommand = new DelegateCommand(a => Results.Clear(), a => Results.Count > 0);
            Batch.CollectionChanged += (s, e) =>
            {
                ExecuteBatchCommand.RaiseCanExecuteChanged();
            };
            Results.CollectionChanged += (s, e) =>
            {
                ClearResultsCommand.RaiseCanExecuteChanged();
            };
        }

        public DelegateCommand ExecuteBatchCommand { get; private set; }

        public DelegateCommand ClearResultsCommand { get; private set; }

        public IEnumerable Queries
        {
            get { return _queries; }
        }

        public ObservableCollection<BatchItem> Batch
        {
            get { return _batch; }
        }

        public ObservableCollection<ResultRow> Results
        {
            get { return _results; }
        }

        public void Execute(BatchItem item)
        {
            ExecuteBatch(item);
        }

        public void ExecuteBatch()
        {
            ExecuteBatch(Batch.ToArray());
            Batch.Clear();
        }

        private void ExecuteBatch(params BatchItem[] items)
        {
            QueryResult qr = null;
            Task.Factory.StartNew(() =>
            {
                var qb = new QueryBatch { Queries = items.Select(i => i.Query).ToArray() };
                using (var client = new StorageClient())
                {
                    client.Open();
                    qr = client.Execute(qb);
                    client.Close();
                }
            }).ContinueWith(t =>
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    if (t.Exception != null)
                    {
                        MessageBox.Show(string.Join(Environment.NewLine, t.Exception.InnerExceptions));
                    }
                    else
                    {
                        foreach (var resultHandler in items.Select(i => i.ResultHandler))
                            resultHandler(qr);
                    }
                }));
            });
        }
    }
}
