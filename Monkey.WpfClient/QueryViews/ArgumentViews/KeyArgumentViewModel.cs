﻿namespace Monkey.WpfClient
{
    public class KeyArgumentViewModel : ViewModel
    {
        private int _value;

        public int Value
        {
            get { return _value; }
            set
            {
                if (value == _value)
                    return;
                _value = value;
                OnPropertyChanged(() => Value);
            }
        }
    }
}
