﻿namespace Monkey.WpfClient
{
    public class ValueArgumentViewModel : ViewModel
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                if (value == _value)
                    return;
                _value = value;
                OnPropertyChanged(() => Value);
            }
        }
    }
}