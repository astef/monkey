using System.Collections;

namespace Monkey.WpfClient
{
    public class DropQueryViewModel : QueryViewModel
    {
        private readonly IEnumerable _arguments;

        private readonly KeyArgumentViewModel _keyVm = new KeyArgumentViewModel();

        public DropQueryViewModel(IUiService uiService)
            : base(uiService)
        {
            _arguments = new object[] { _keyVm };
        }

        public override string QueryName
        {
            get { return "Drop"; }
        }

        public override string QueryDescription
        {
            get { return "Removes specified key and associated value"; }
        }

        public override IEnumerable Arguments
        {
            get { return _arguments; }
        }

        protected override BatchItem GetBatchItem()
        {
            var query = new DropQuery { Key = _keyVm.Value };
            return new BatchItem
            {
                ResultHandler = (res) =>
                {
                    UiService.Results.Remove(new ResultRow(query.Key));
                },
                Query = query,
                StringRepresentation = QueryName + " " + _keyVm.Value
            };
        }
    }
}