using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Monkey.WpfClient.Annotations;

namespace Monkey.WpfClient
{
    public class GetAllKeysQueryViewModel : QueryViewModel
    {
        private readonly IEnumerable _arguments;

        public GetAllKeysQueryViewModel([NotNull] IUiService uiService)
            : base(uiService)
        {
            _arguments = new object[0];
        }

        public override string QueryName
        {
            get { return "Get all keys"; }
        }

        public override string QueryDescription
        {
            get { return "Returns all avaible keys"; }
        }

        public override IEnumerable Arguments
        {
            get { return _arguments; }
        }

        protected override BatchItem GetBatchItem()
        {
            return new BatchItem
            {
                Query = new GetAllKeysQuery(),
                ResultHandler = qr =>
                {
                    var keys = new HashSet<int>(qr.Records.Where(r => r.State != RecordState.Deleted).Select(r => r.Key));
                    foreach (var resRow in UiService.Results.ToArray().Where(resRow => !keys.Contains(resRow.Key)))
                        UiService.Results.Remove(resRow);
                    foreach (var newRow in keys.Select(k => new ResultRow(k)).Where(newRow => !UiService.Results.Contains(newRow)))
                        UiService.Results.Add(newRow);
                },
                StringRepresentation = QueryName
            };
        }
    }
}