﻿using System.Collections;
using System.Linq;

namespace Monkey.WpfClient
{
    public class GetQueryViewModel : QueryViewModel
    {
        private readonly IEnumerable _arguments;

        private readonly KeyArgumentViewModel _keyVm = new KeyArgumentViewModel();

        public GetQueryViewModel(IUiService uiService)
            : base(uiService)
        {
            _arguments = new object[] { _keyVm };
        }

        public override string QueryName
        {
            get { return "Get"; }
        }

        public override string QueryDescription
        {
            get { return "Returns a value associated with the key"; }
        }

        public override IEnumerable Arguments
        {
            get { return _arguments; }
        }

        protected override BatchItem GetBatchItem()
        {
            var query = new GetQuery { Key = _keyVm.Value };
            return new BatchItem
            {
                Query = query,
                ResultHandler = qr =>
                {
                    var requestedBytes = qr.Records.Where(r => r.Key == query.Key && r.State != RecordState.Deleted).Select(r => r.Value).FirstOrDefault();
                    if (requestedBytes == null)
                        return;
                    var requestedValue = FormatHelper.Deserialize(requestedBytes);
                    var newRow = new ResultRow(query.Key, requestedValue);
                    var existingIndex = UiService.Results.IndexOf(newRow);
                    if (existingIndex == -1)
                        UiService.Results.Add(newRow);
                    else
                        UiService.Results[existingIndex].Value = requestedValue;
                },
                StringRepresentation = QueryName + " " + _keyVm.Value
            };
        }
    }
}
