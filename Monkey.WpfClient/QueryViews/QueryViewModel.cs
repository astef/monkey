﻿using System;
using System.Collections;
using Monkey.WpfClient.Annotations;

namespace Monkey.WpfClient
{
    public abstract class QueryViewModel : ViewModel
    {
        protected readonly IUiService UiService;

        protected QueryViewModel([NotNull] IUiService uiService)
        {
            if (uiService == null)
                throw new ArgumentNullException("uiService");
            UiService = uiService;
            ExecuteQueryCommand = new DelegateCommand(a => { OnExecute(); });
            BatchQueryCommand = new DelegateCommand(a => OnBatch());
        }

        public abstract string QueryName { get; }

        public abstract string QueryDescription { get; }

        public abstract IEnumerable Arguments { get; }

        public DelegateCommand ExecuteQueryCommand { get; private set; }

        public DelegateCommand BatchQueryCommand { get; private set; }

        protected abstract BatchItem GetBatchItem();

        private void OnExecute()
        {
            UiService.Execute(GetBatchItem());
        }

        private void OnBatch()
        {
            UiService.Batch.Add(GetBatchItem());
        }
    }
}
