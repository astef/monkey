using System.Collections;

namespace Monkey.WpfClient
{
    public class SetQueryViewModel : QueryViewModel
    {
        private readonly IEnumerable _arguments;
        private readonly KeyArgumentViewModel _keyVm = new KeyArgumentViewModel();
        private readonly ValueArgumentViewModel _valueVm = new ValueArgumentViewModel();

        public SetQueryViewModel(IUiService uiService)
            : base(uiService)
        {
            _arguments = new object[] { _keyVm, _valueVm };
        }

        public override string QueryName
        {
            get { return "Set"; }
        }

        public override string QueryDescription
        {
            get { return "Sets the value associated with the key"; }
        }

        public override IEnumerable Arguments
        {
            get { return _arguments; }
        }

        protected override BatchItem GetBatchItem()
        {
            var value = _valueVm.Value ?? "";
            var query = new SetQuery { Key = _keyVm.Value, Value = FormatHelper.Serialize(value) };

            return new BatchItem
            {
                Query = query,
                ResultHandler = qr =>
                {
                    var newRow = new ResultRow(query.Key, value);
                    var index = UiService.Results.IndexOf(newRow);
                    if (index < 0)
                        UiService.Results.Add(newRow);
                    else
                        UiService.Results[index].Value = value;
                },
                StringRepresentation = QueryName + " " + _keyVm.Value
            };
        }
    }
}