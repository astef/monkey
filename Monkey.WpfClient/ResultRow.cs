﻿namespace Monkey.WpfClient
{
    public class ResultRow : ViewModel
    {
        private string _value;

        public ResultRow(int key, string value = null)
        {
            Key = key;
            Value = value;
        }

        public int Key { get; private set; }

        public string Value
        {
            get { return _value; }
            set
            {
                if (value == _value)
                    return;
                _value = value;
                OnPropertyChanged(() => Value);
            }
        }

        public override bool Equals(object obj)
        {
            var p = obj as ResultRow;
            if (p == null)
                return false;
            return Key == p.Key;
        }

        public override int GetHashCode()
        {
            return Key;
        }
    }
}