﻿using System.ServiceModel;

namespace Monkey
{
    [ServiceContract]
    public interface IStorage
    {
        [OperationContract]
        QueryResult Execute(QueryBatch batch);
    }
}
