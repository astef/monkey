using System.Runtime.Serialization;

namespace Monkey
{
    [DataContract]
    public class DropQuery : Query
    {
        [DataMember]
        public int Key { get; set; }
    }
}