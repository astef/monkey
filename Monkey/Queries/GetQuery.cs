﻿using System.Runtime.Serialization;

namespace Monkey
{
    [DataContract]
    public class GetQuery : Query
    {
        [DataMember]
        public int Key { get; set; }
    }
}
