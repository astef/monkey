using System.Runtime.Serialization;

namespace Monkey
{
    [DataContract]
    public class SetQuery : Query
    {
        [DataMember]
        public int Key { get; set; }

        [DataMember]
        public byte[] Value { get; set; }
    }
}