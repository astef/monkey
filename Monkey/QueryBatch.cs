using System.Runtime.Serialization;

namespace Monkey
{
    [DataContract]
    public class QueryBatch
    {
        [DataMember]
        public Query[] Queries { get; set; }
    }
}