using System.Runtime.Serialization;

namespace Monkey
{
    [DataContract]
    public class QueryResult
    {
        [DataMember]
        public Record[] Records { get; set; }
    }
}