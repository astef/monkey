using System.Runtime.Serialization;

namespace Monkey
{
    [DataContract]
    public class Record
    {
        [DataMember]
        public RecordState State { get; set; }

        [DataMember]
        public int Key { get; set; }

        [DataMember]
        public byte[] Value { get; set; }
    }
}