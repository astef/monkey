namespace Monkey
{
    public enum RecordState
    {
        NotChanged = 0,
        Set = 100,
        Deleted = 200
    }
}