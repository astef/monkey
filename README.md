### Key-value storage done during the weekend (up to monday) ###

Study project which helped me to gain experience in network programming with WCF.

It is a simple key-value storage available through a network (WCF service), with a WPF client application.

It supports operation batching and multiple simultaneous connections (thread-safe).
